package com.example.kevincabrera.examen

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer

import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeGuessTextView: TextView
    internal lateinit var startButton: Button
    internal lateinit var stopButton: Button

    internal  var  n = 0
    internal var score = 0;
    internal lateinit var countDownTimer: CountDownTimer
    internal var countDownInterval = 1000L;
    internal val initialCountDown = 10000L;
    internal var timeLeft = 10;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gameScoreTextView = findViewById(R.id.textView_puntajeTotal)
        timeGuessTextView = findViewById(R.id.txtView_Time)
        startButton = findViewById(R.id.button_startTime)
        stopButton = findViewById(R.id.button_stopTime)

        startButton.setOnClickListener {_ -> startGame()
        }
        stopButton.setOnClickListener {_ -> incrementScore(10-timeLeft,n)
        }
    }

private fun startGame(){
    restartTimeGame()
    countDownTimer = object : CountDownTimer(initialCountDown,countDownInterval){
        override fun onTick(millisUntilFinished: Long) {
            timeLeft = millisUntilFinished.toInt()/1000
        }
        override fun onFinish() {
           // Toast.makeText(this,"Too slow baby",Toast.LENGTH_LONG).show()
        }
    }
    countDownTimer.start()
}

    private fun restartTimeGame(){
        n = (1..10).shuffled().first()
        val randTime = getString(R.string.backgrundTime , Integer.toString(n))
        timeGuessTextView.text =   randTime
    }

    private fun incrementScore(tapTime: Int, randomTime: Int){
        if(tapTime == randomTime){
            score = score +100;
        }
        else if(Math.abs(tapTime-randomTime)==1){
            score = score + 50;
        }
        actualizarScoreView()
    }

    private fun actualizarScoreView(){
        val totalPoints = getString(R.string.totalScore , Integer.toString(score))
        gameScoreTextView.text = totalPoints
    }
}
